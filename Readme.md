## Create and Activate Virtual Environment for Python 3.6
```python3 -m venv .venv```
```source .venv/bin/activate```

## Install pip dependencies
```pip3.6 install -r requirements.txt```

## Run Tests
``` pytest ```

# Configure Application
```Copy sample.env to .env and replace environment variable placeholder```

## Run Application
```flask run ```

## API Documentation
The API can be accessed on http://localhost:5000/swagger