from flask import Flask
from flask_swagger_ui import get_swaggerui_blueprint
import os
from yaml import Loader, load

def get_application(name):
    app = create_application(name)
    register_extensions(app)
    initialize_swagger_blueprint(app)
    return app

def create_application(name):
    return Flask(name)

def register_extensions(app):
    from extensions import db
    app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['SQLALCHEMY_DATABASE_URI']
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)

def initialize_swagger_blueprint(app):
    swagger_file_path = f'docs/swagger.yaml'
    swagger_url = '/swagger'
    if os.path.isfile(swagger_file_path):
        swagger_yml = load(open(swagger_file_path, 'r'), Loader=Loader)
        swagger_blueprint = get_swaggerui_blueprint(f'{swagger_url}', swagger_file_path, config = {'spec': swagger_yml})
        app.register_blueprint(swagger_blueprint, url_prefix=swagger_url)