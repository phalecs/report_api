from app import get_application, register_extensions
from flask import request, jsonify, json, render_template
from flask_weasyprint import HTML, render_pdf
from models import Report


app = get_application(__name__)

@app.route("/report/<int:report_id>")
def get_report_by_id(report_id):
    """
        Default Content Type is application/json.
        Also supported: xml and pdf
    """

    report = Report.query.get_or_404(report_id)

    requested_content_type = request.headers.get('Accept', "application/json")
    
    if not report.valid:
        return "Invalid Report", 400

    if requested_content_type.endswith("xml"):   
        return report.xml, 200,  {'Content-Type': 'application/xml'}
    elif "application/pdf" in requested_content_type:    
        return render_pdf(report.html)
    else:
        return jsonify(report.json)