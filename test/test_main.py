
import pytest

from extensions import db
from main import app
from models import Report

@pytest.fixture
def test_app():
    
    app.config['TESTING'] = True
    yield app


@pytest.fixture
def client(test_app):
    client = test_app.test_client()
    yield client

@pytest.fixture
def test_db(test_app):

    with test_app.app_context():
        db.init_app(test_app)
        db.create_all()
        yield db
        db.drop_all()
    
@pytest.fixture
def test_report(test_db):
    report = Report()
    report.type = '''{"organization":"MOM Corp.","reported_at":"3015-08-24","created_at":"3015-08-23","inventory":[{"name":"bending unit","price":"2000.00"},{"name":"stapling unit","price":"50.00"}]}'''
    test_db.session.add(report)
    test_db.session.commit()

    yield report

    test_db.session.delete(report)
    test_db.session.commit()

@pytest.fixture
def test_invalid_report(test_db):
    report = Report()
    report.type = '''{"organization":"MOM Corp.","reported_at":"3015-08-24",'''
    test_db.session.add(report)
    test_db.session.commit()

    yield report

    test_db.session.delete(report)
    test_db.session.commit()


def test_report_not_found(client, test_db):
    """Start with a blank database."""

    rv = client.get('/report/100')

    assert rv.status_code == 404
    print(rv.content_type)

def test_report_get_report_as_json(client, test_report):
    rv = client.get(f'/report/{test_report.id}')

    assert rv.status_code == 200
    assert rv.content_type == 'application/json'
    assert rv.json.keys() == test_report.json.keys()

def test_report_get_report_as_pdf(client, test_report):
    rv = client.get(f'/report/{test_report.id}', headers={
        'Accept': 'application/pdf'
    })

    assert rv.status_code == 200
    assert rv.content_type == 'application/pdf'

def test_report_get_report_as_xml(client, test_report):
    rv = client.get(f'/report/{test_report.id}', headers={
        'Accept': 'application/xml'
    })

    assert rv.status_code == 200
    assert rv.content_type == 'application/xml'

    assert rv.get_data(as_text=False) == test_report.xml

def test_report_get_report_with_invalid_json(client, test_invalid_report):
    rv = client.get(f'/report/{test_invalid_report.id}')

    assert rv.status_code == 400
    assert rv.get_data(as_text=True) == 'Invalid Report'


    


