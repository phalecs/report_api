from extensions import db
from dicttoxml import dicttoxml
from flask import json, render_template
from flask_weasyprint import HTML

class Report(db.Model):
    __tablename__ = "reports"

    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String)

    @property
    def valid(self):
        try:
            self.to_dict
            return True
        except ValueError:
            return False

    @property
    def to_dict(self):
        response_dict = json.loads(self.type)
        response_dict['id'] = self.id
        return response_dict

    @property
    def json(self):
        return self.to_dict
    
    @property
    def xml(self):
        response_xml = dicttoxml(self.to_dict)
        return response_xml

    @property
    def html(self):
        response_html = render_template('pdf_template.html', report=self.to_dict)
        return HTML(string=response_html)
